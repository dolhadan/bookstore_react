/**
 * Created by DolhaDan on 7/18/17.
 */
import {
    ADD_TO_CART,
    REMOVE_FROM_CART,
    CHECKOUT_REQUEST,
    CHECKOUT_SUCCESS
} from './ActionsTypes';

export const addBookToCart=(book)=>{
    return {
        type:ADD_TO_CART,
        book:book
    }
};


export const removeFromCart=(book)=>{
    return{
        type:REMOVE_FROM_CART,
        book:book
    }
};
export const actions={
    addBookToCart:(book)=>addBookToCart(book),
    removeFromCart:(book)=>removeFromCart(book),
};

