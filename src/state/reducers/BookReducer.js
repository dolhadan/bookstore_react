/**
 * Created by DolhaDan on 7/18/17.
 */
import {
    ADD_TO_CART,
    REMOVE_FROM_CART
} from '../ActionsTypes';


export const bookReducer = (state = {}, action) => {
    let newState={};
    switch (action.type) {
        case ADD_TO_CART:
            newState.books = state.books.filter(book => book.id !== action.book.id);
             newState.books = newState.books.concat(action.book);
            return newState;
        case REMOVE_FROM_CART:
             newState = state.filter(book => book.id !== action.book.id);
            return newState;
        default:
            return state;
    }
};