/**
 * Created by DolhaDan on 7/18/17.
 */
import {createStore,bindActionCreators} from 'redux';
import {bookReducer} from './reducers/BookReducer';
import * as bookActions from './Actions';

const defaultState = {
    books: [],
};

export const configureStore = () => {
    const store = createStore(bookReducer, defaultState);
    const actions = {
        bookAction: bindActionCreators(bookActions.actions,
            store.dispatch),
    };
    return {store,actions};
};

export default configureStore;


