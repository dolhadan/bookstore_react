/**
 * Created by DolhaDan on 7/18/17.
 */
export const ADD_TO_CART="ADD_TO_CART";
export const REMOVE_FROM_CART="REMOVE_FROM_CART";
export const CHECKOUT_REQUEST="CHECKOUT_REQUEST";
export const CHECKOUT_SUCCESS="CHECKOUT_SUCCESS";