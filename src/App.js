import React, {Component} from 'react';
import {Button} from 'react-materialize';
import './App.css';
import {BooksListContainer} from './books_components/BooksListContainer';
import {BrowserRouter as Router, Route, Link, Redirect} from 'react-router-dom';
import {AppBar} from './AppBar';
import {Contact} from './contact_components/Contact';
import {BookViewContainer} from './books_components/BookViewContainer';
import {connect} from 'react-redux';
import {ShoppingCartContainer} from './cart_components/ShoppingCart';


export class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            extended: false,
        };
        this.handleOnAddToCart = this.handleOnAddToCart.bind(this);
    }

    handleOnAddToCart(book) {
        // var books = this.state.booksInCart;
        // console.log(books.indexOf(book));
        // books.push(book);
        // this.setState({
        //     booksInCart: books,
        // });
        this.props.actions.bookAction.addBookToCart(book);

    }


    render() {
        const nrOfItemsInCart = this.props.nrOfItemsInCart;
        const createElement = (component, props) => {
            return <Component actions={this.props.actions}
                              {...props}/>
        };
        return (
            <Router createElement={createElement}>
                <div>
                    <AppBar nrOfItemsInCart={nrOfItemsInCart}/>
                    <Route exact path="/"
                           component={(props) => <BooksListContainer {...props} grid={this.state.extended}/>}/>
                    <Route path="/contact" component={(props) => <Contact {...props} />}/>
                    <Route path='/book/:bookID?/' component={(props) => <BookViewContainer {...props}
                                                                                           handleOnAddToCart={this.handleOnAddToCart}/>}/>
                    <Route path="/cart" component={(props)=><ShoppingCartContainer {...props} booksInCart={this.props.booksInCart}/>}/>
                </div>
            </Router>


        );
    }
}


const mapStatetoProps=(state)=>{
    return{
        booksInCart: state.books,
        nrOfItemsInCart: state.books.length,
    };
};
export default connect(
    mapStatetoProps
)(App);
