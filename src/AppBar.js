/**
 * Created by Dan on 13.07.2017.
 */

import React from 'react';
import {Icon} from 'react-materialize';
import './App.css';
import {Link} from 'react-router-dom';


export const AppBar= (props)=>{
    return (
        <div className="topnav">
            <Link to="/" className="logo">
                    <Icon>
                        home
                    </Icon>
            </Link>
            <Link to="/contact" className="logo right">
                <Icon>
                    info
                </Icon>

            </Link>
             <Link to="/cart" className="logo right">
                    <Icon >
                        shopping_cart
                    </Icon>
                 {props.nrOfItemsInCart!=0 ? props.nrOfItemsInCart : null}
            </Link>



        </div>);
};
