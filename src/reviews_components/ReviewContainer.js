/**
 * Created by DolhaDan on 7/18/17.
 */
import React,{Component} from 'react';
import {ReviewList} from './ReviewList';
import '../styles/reviewStyle.css';
const url=(id)=> `http://localhost:8000/review/${id}/`;
export class ReviewContainer extends Component{

    constructor(props){
        super(props);
        this.state={
            reviews:[],
        };
    }
     onSuccessCallback(data) {
        this.setState({

            reviews: data,
        });
    }

    fetchReviews(bookID){
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        const init = {
            method: "GET",
            mode: 'cors',
            headers: headers,
            cache: 'default',
        };

        fetch(url(bookID),init)
            .then((response) => {
                if (response.status === 401) {
                    console.log("eroare");
                    throw Error();
                }
                return response.json();
            })
            .then((data) => {
                console.log(data);
                this.onSuccessCallback(data);
            })
            .catch(() => {
                console.log("exception");
            });
    }

    componentWillMount(){
        const bookID=this.props.bookID;
        this.fetchReviews(bookID);

    }



    render(){
        return(
            <div className="max-width">
                <h4>Reviews</h4>
                <div className="line"/>
                {this.state.reviews.length!=0 ? <ReviewList reviews={this.state.reviews}/> : <div> No reviews for this book </div>}
            </div>
        );
    }
}