/**
 * Created by DolhaDan on 7/18/17.
 */
import React from 'react';
import '../styles/reviewStyle.css';
export const ReviewPane=(props)=>{
    const {author,content}=props.review;
    return(
        <div>
            <h5>{author}: </h5>

            <p>
                {content}
            </p>
        </div>
    )
};