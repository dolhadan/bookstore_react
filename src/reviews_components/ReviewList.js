/**
 * Created by DolhaDan on 7/18/17.
 */
import {ReviewPane} from './ReviewPane';
import React from 'react';
export const ReviewList = (props) => {
    const reviewsList = props.reviews.map(review => {
        return (<ReviewPane review={review}/>)
    });
    return (
        <div>
            {props.review !==null ?reviewsList : null}
        </div>);
};