import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Button from 'react-materialize';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import configureStore from "./state/Store";
import {Provider} from 'react-redux';


const {store, actions}=configureStore();

ReactDOM.render(
    <Provider store={store}>
        <App actions={actions}/>
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();
