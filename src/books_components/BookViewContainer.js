/**
 * Created by DolhaDan on 7/17/17.
 */
import React, {Component} from 'react';
import {BookView} from '../books_components/BookView';
const urlRoot = 'http://localhost:8000/books/';
export class BookViewContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            book: null,
            inCart: false,
        };

    }

    fetchBook(id) {
        const url = urlRoot + id + '/';
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        const init = {
            method: "GET",
            mode: 'cors',
            headers: headers,
            cache: 'default',
        };

        fetch(url, init)
            .then((response) => {
                if (response.status === 401) {
                    console.log("eroare");
                    throw Error();
                }
                return response.json();
            })
            .then((data) => {
                console.log(data);
                this.onSuccessCallback(data);
            })
            .catch(() => {
                console.log("exception");
            });
    }

    onSuccessCallback(data) {
        this.setState({
            book: data,
        });
    }

    componentWillMount() {
        const id = this.props.match.params.bookID;
        this.fetchBook(id);
    }

    handleOnAddToCart(book) {
        if (!this.state.inCart) {
            this.props.handleOnAddToCart(book);
            this.setState((prevState, props) => {
                return {
                    inCart: !prevState.inCart,
                }

            });
        }
    }


    render() {

        const book = this.state.book;
        return (
            <div>
                {book != null ? <BookView book={book} handleOnAddToCart={this.handleOnAddToCart.bind(this)}/> : null }

            </div>
        );
    }
}