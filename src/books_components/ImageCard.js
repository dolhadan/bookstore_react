/**
 * Created by DolhaDan on 7/14/17.
 */
import '../styles/bookStyle.css'
import React from 'react';
import {Component} from "react";
import {Link} from 'react-router-dom';


export class ImageCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFlipped:false,
            isExpendend:false,
        };
    }

    render() {
        const id=this.props.bookID;
        const url=`/book/${id}`;
        return (
            <Link to={url}>
                <img className="img card" src={this.props.imagePath} />
            </Link>
        );
    }
}