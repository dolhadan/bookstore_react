/**
 * Created by Dan on 11.07.2017.
 */
import React, {Component} from 'react';
import '../styles/bookStyle.css';
import {BookPane} from './BookPane';
import {ImageCard} from './ImageCard'
export class BooksList extends Component {

    render(){
        const books=this.props.books;
        const grid=this.props.grid;
        const bookPanes=books.map((book)=>{
            if(!grid)
               return <BookPane book={book}/>;
            return <ImageCard imagePath={book.image_path} titlu={book.title} author={book.author} description={book.description} bookID={book.id} />;}

            );
        return(
          <div className="flex-container">

              {bookPanes}
          </div>
        );
    }

}