/**
 * Created by Dan on 11.07.2017.
 */
import React, {Component} from 'react';
import {UnmountClosed as Collapse} from 'react-collapse';
import Reveal from 'react-reveal'; // this package

import {Icon} from 'react-materialize'

import '../styles/bookStyle.css'
import {BookDetails} from "./BookDetails";

export class BookPane extends Component{
   render() {
        const book=this.props.book;
        return (

            <div className="card card_width">
                <img className="img" src={book.image_path}/>
                <BookDetails book={book}/>
            </div>
             );
         }
}