/**
 * Created by DolhaDan on 7/17/17.
 */
import React, {Component} from 'react';
import '../styles/bookViewStyle.css';
import '../styles/bookStyle.css';
import {BookDetails} from "./BookDetails";
import {ReviewContainer} from "../reviews_components/ReviewContainer";

export class BookView extends Component {

    constructor(props) {
        super(props);

    }

    render() {
        const book = this.props.book;
        return (
            <div className="content">
                <div className="content_book">
                    <div>
                        <img className="img" src={book.image_path}/>
                        <button className="button" onClick={() => this.props.handleOnAddToCart(book)}>
                            Add To Cart
                        </button>
                    </div>
                    <div className="details">
                        <BookDetails book={book}/>
                    </div>
                </div>
                <div className="content_reviews">
                     <ReviewContainer bookID={book.id}/>
                </div>
            </div>);
    };
}

