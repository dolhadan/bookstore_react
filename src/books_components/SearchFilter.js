/**
 * Created by Dan on 11.07.2017.
 */
import React,{Component} from 'react'

export class SearchFilter extends Component{
    constructor(props){
        super(props);
        this.handleFilterTextInputChange=
            this.handleFilterTextInputChange.bind(this);

    }
    handleFilterTextInputChange(e){
        this.props.onFilterTextInput(e.target.value);
    }
    onSubmitChange(e){
        e.preventDefault();
    }
    render() {
    return (
      <form className="search_form" onSubmit={this.onSubmitChange.bind(this)}>
        <input className="center"
            type="text"
            placeholder="Search..."
            value={this.props.filterText}
            onChange={this.handleFilterTextInputChange}
        />
        <p>
            {' '}
         </p>
      </form>
    );
  }

}