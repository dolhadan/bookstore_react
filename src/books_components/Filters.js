/**
 * Created by Dan on 12.07.2017.
 */
import React, {Component} from 'react'


export class Filters extends Component {
    constructor(props) {
        super(props);
        this.handleInputChange =
        this.handleInputChange.bind(this);

    }

    handleInputChange(e) {
        this.props.onCheckedFilters(e.target.value);
    }

    render() {
        let x = false;
        return (
            <div>
                <form>
                    <label>
                        Price: 0 - 9.99
                        <input
                            name="isFirst"
                            type="checkbox"
                            onChange={this.handleInputChange}/>
                    </label>

                </form>
            </div>
        );
    }
}