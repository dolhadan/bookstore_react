/**
 * Created by DolhaDan on 7/17/17.
 */
import React,{Component} from 'react';
import {Icon} from 'react-materialize';
import '../styles/bookStyle.css';
import '../App.css'

export class  BookDetails extends Component{
    constructor(props) {
        super(props);
        this.state={
            isExpendend: false,
        };
        this.handleExpand=this.handleExpand.bind(this);
    }
    renderDescription(){
        const description=this.props.book.description;
        if(this.state.isExpendend)
            return (

                        <p className="App-intro">
                           <div dangerouslySetInnerHTML={{__html: description}}/>
                         </p>

            );
        return <p className="App-intro" dangerouslySetInnerHTML={{__html: description.substring(0,300)+'...'}}/>
    }
    handleExpand(){
        const notExpanded=!this.state.isExpendend;
        this.setState({
            isExpendend:notExpanded,
        });
    }
    render(){
        const book=this.props.book;
        return(
                <div className="container">
                    <h4><b>{book.title}</b></h4>
                        <p>{book.author}</p>
                    {this.renderDescription()}
                <button className="expand_button" onClick={this.handleExpand}>
                    <Icon>
                        more_vert
                    </Icon>
                </button>

                </div>
        );
    }
}
