/**
 * Created by Dan on 11.07.2017.
 */
import React,{Component} from 'react';
import ReactLoading from 'react-loading';
import {BooksList} from "./BooksList";
import {SearchFilter} from "./SearchFilter";
import {Icon} from 'react-materialize';
import {Filters} from "./Filters";

export class BooksListContainer extends Component{
     constructor(props) {
        super(props);
        this.state = {
            books: [],
            searchedText: '',
            checked: false,
            grid:true,
        };

       this.handleFilterTextInput = this.handleFilterTextInput.bind(this);
       this.onCheckedFilters=this.onCheckedFilters.bind(this);
    }
    onCheckedFilters(checked){
         this.setState({
             checked:checked,
         })
    }
    handleFilterTextInput(filterText){
        this.fetchBooks("http://127.0.0.1:8000/result/?search=",filterText);
         this.setState({
             searchedText:filterText
         });
    }

    fetchBooks(url,filterText) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        const init = {
            method: "GET",
            mode: 'cors',
            headers: headers,
            cache: 'default',
        };

        fetch(url+filterText, init)
            .then((response) => {
                if (response.status === 401) {
                    console.log("eroare");
                    throw Error();
                }
                return response.json();
            })
            .then((data) => {
                console.log(data);
                this.onSuccessCallback(data);
            })
            .catch(() => {
                console.log("exception");
            });
    }

    onSuccessCallback(data) {
        this.setState({

            books: data,
        });
    }

    componentWillMount() {
        this.fetchBooks("http://127.0.0.1:8000/books/",'');
    }
    handleChangeLayout(){
        this.setState((prevState,props)=> {
            return{
            grid: !prevState.grid,
            }

        });

    }

    render(){

         const booksContainer=(this.state.books ? <BooksList books={this.state.books} grid={this.state.grid}/> : <ReactLoading type="sping" color="#444"/>);
         return(
             <div className="App">

                 {/* <Filters onCheckedFilters={this.onCheckedFilters} cheked={this.state.checked}/>*/}
                 <div>
                     <div>
                        <SearchFilter
                             filterText={this.state.searchedText}
                            onFilterTextInput={this.handleFilterTextInput}
                        />
                         <button className="expand_button" onClick={this.handleChangeLayout.bind(this)}>
                            <Icon>
                                {this.state.grid ?  'view_agenda' : 'view_comfy'}
                            </Icon>
                </button>
                     </div>

                 {booksContainer}
                 </div>
             </div>
         );

    }
}