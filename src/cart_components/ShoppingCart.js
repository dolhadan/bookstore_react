/**
 * Created by DolhaDan on 7/18/17.
 */
import React, {Component} from 'react';
import {BookInCart} from './BookInCart';
import '../App.css';
import {Icon} from 'react-materialize';
export class ShoppingCartContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            counter: 0,
        }
        this.handleBack = this.handleBack.bind(this);
        this.handleForward = this.handleForward.bind(this);
    }

    handleBack() {
        const newCounter = this.state.counter - 1;
        this.setState({
            counter: newCounter,
        });
    }

    handleForward() {
        const newCounter = this.state.counter + 1;
        this.setState({
            counter: newCounter,
        });
    }

    render() {
        const book = this.props.booksInCart[this.state.counter];
        return (
            <div className="content_cart">
                <div className="preview_cart">

                    {this.props.booksInCart[this.state.counter - 1] != null ?
                        <a className="arrow arrow_left active_arrow" onClick={this.handleBack}>
                            <Icon>
                                keyboard_arrow_left
                            </Icon>
                        </a>
                        :
                        <div className="arrow arrow_left">
                                <Icon>
                                    keyboard_arrow_left
                                </Icon>
                            </div>
                    }
                    {

                    }
                    {book != null ? <BookInCart book={book}/> : null}
                    {
                        this.props.booksInCart[this.state.counter + 1] != null ?

                            <a className="arrow arrow_right active_arrow" onClick={this.handleForward}>
                                <Icon>
                                    keyboard_arrow_right
                                </Icon>
                            </a>
                            :
                            <div className="arrow arrow_right">
                                <Icon>
                                    keyboard_arrow_right
                                </Icon>
                            </div>
                    }
                </div>
            </div>

        );
    }
}

