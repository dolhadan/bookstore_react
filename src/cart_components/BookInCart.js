/**
 * Created by DolhaDan on 7/18/17.
 */
import React from 'react';
import '../styles/shoppingCartStyle.css'
export const BookInCart = (props) => {
    const {title, author, image_path} = props.book;
    return (
        <div className="content_book_preview">
            <img src={image_path} className="image_cart"/>
            <h4>{title}</h4>
            <h6>{author}</h6>
        </div>)
};